#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QIntValidator>
#include <QMainWindow>
#include <vector>
#include <string>
#include "worker.h"
#include "taskselector.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

    private:
        Ui::MainWindow *ui;
        Worker worker;
        QIntValidator* port_checker = nullptr;
        QIntValidator* timeout_checker = nullptr;

        void print_callback(const std::vector<std::string>& info);
        void set_active_workers(int workers_num, int tasks_num);

    private slots:
        void create_server_click();
        void add_task_click();
};

#endif // MAINWINDOW_H
