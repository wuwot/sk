#ifndef CORO_H
#define CORO_H

// dark vodoo magic
// use with caution!

// simple reentrant stackless coroutines

using coro_ctx = unsigned;
constexpr unsigned coro_init = 0;

#define CORO_BEGIN(context) coro_ctx& local_ctx_ = context; switch (local_ctx_) { case 0:
#define CORO_END() default:;} local_ctx_ = 0

#define CORO_YIELD() do{ local_ctx_ = __LINE__; return; case __LINE__:; }while(0)
#define CORO_ABORT() do{ local_ctx_ = 0; return; }while(0)

#endif // CORO_H
