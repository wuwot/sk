#ifndef TASKSELECTOR_H
#define TASKSELECTOR_H

#include <QDialog>

namespace Ui {
class TaskSelector;
}

class TaskSelector : public QDialog
{
    Q_OBJECT

    public:
        explicit TaskSelector(QWidget *parent = 0);
        ~TaskSelector();

        std::string v_prog_path;
        std::string v_cmd_line;
        std::string v_task_name;

    private:
        Ui::TaskSelector *ui;

    private slots:
        void accept_click();
        void select_prog_click();
};

#endif // TASKSELECTOR_H
