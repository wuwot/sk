#include <fcntl.h>
#include <unistd.h>
#include <iostream>
#include <tuple>
#include <algorithm>
#include "scheduler.h"
#include "coro.h"
#include "server.h"

std::array<char, 12> create_cmd(char cmd, int task_id = 0, int param2 = 0, int param3 = 0)
{
    std::array<char, 12> ret;

    ret[0] = cmd;
    ret[1] = (task_id >> 16) & 0xFF;
    ret[2] = (task_id >> 8) & 0xFF;
    ret[3] = task_id & 0xFF;
    ret[4] = (param2 >> 24) & 0xFF;
    ret[5] = (param2 >> 16) & 0xFF;
    ret[6] = (param2 >> 8) & 0xFF;
    ret[7] = param2 & 0xFF;
    ret[8] = (param3 >> 24) & 0xFF;
    ret[9] = (param3 >> 16) & 0xFF;
    ret[10] = (param3 >> 8) & 0xFF;
    ret[11] = param3 & 0xFF;

    return ret;
}

std::tuple<char, int, int, int> read_cmd(const std::array<char, 12>& buffer)
{
    const unsigned char* buf = reinterpret_cast<const unsigned char*>(buffer.data());

    char cmd = buf[0];
    int task_id = (buf[1] << 16) | (buf[2] << 8) | buf[3];
    int param2 = (buf[4] << 24) | (buf[5] << 16) | (buf[6] << 8) | buf[7];
    int param3 = (buf[8] << 24) | (buf[9] << 16) | (buf[10] << 8) | buf[11];

    return std::make_tuple(cmd, task_id, param2, param3);
}

void Scheduler::schedule_task(int task_id)
{
    if (workers.size() > 0)
    {        
        auto task_it = tasks.find(task_id);

        if (task_it != tasks.end())
        {
            auto it = std::next(std::begin(workers), rand() % workers.size());
            int worker = it->first;
            it->second.tasks.push_back(task_id);

            auto& data = task_it->second;

            std::cout << "scheduling new task (task_id: " << task_id << ") to worker: " << worker << std::endl;

            int cmd_line_size = data.cmd_line.size();
            int program_size = lseek(data.program, 0, SEEK_END);
            lseek(data.program, 0, SEEK_SET);

            s.send(worker, create_cmd(SENDING_TASK, task_id, cmd_line_size, program_size));
            s.send(worker, data.cmd_line.c_str(), cmd_line_size);
            s.send(worker, data.program, program_size);
        }
    }
}

std::pair<int, int> Scheduler::incoming_data_handler(int connection_id, const char* buffer, int data_len)
{
    auto it = workers.find(connection_id);

    std::cout << "connection: " << connection_id << " send data to server" << std::endl; //TODO: debug cout

    if (it == workers.end()) //handle unknown connection
    {
        int already_read = 0;
        auto it2 = unknown_workers.find(connection_id);

        if (it2 == unknown_workers.end())
        {
            auto ret = unknown_workers.insert(std::make_pair(connection_id, new_worker{}));
            it2 = ret.first;
        }

        if (!it2->second.rec_pass) //first will come command with password length
        {
            char cmd = 0;
            int task_id, param1, param2;
            int bytes_to_read = std::min(12 - it2->second.tmp_pos, data_len);

            for (int i = it2->second.tmp_pos; i < bytes_to_read; ++i, ++already_read, ++it2->second.tmp_pos)
                it2->second.tmp_buffer[i] = buffer[i];

            if (it2->second.tmp_pos == 12)
            {
                std::tie(cmd, task_id, param1, param2) = read_cmd(it2->second.tmp_buffer);
                it2->second.tmp_pos = 0;
                it2->second.rec_pass = true;
                it2->second.pass_len = std::min(param1, 32);

                it2->second.pass.reserve(param1);
            }
        }

        if (it2->second.rec_pass) //now receive password
        {
            auto cap = it2->second.pass_len;
            auto len = it2->second.pass.length();

            for (unsigned i = 0; i < std::min(cap - len, (long unsigned)data_len - already_read); ++i)
                it2->second.pass += buffer[i + already_read];
        }

        if (it2->second.pass_len == it2->second.pass.length() && it2->second.pass.length() > 0)
        {
            std::string pass = it2->second.pass;
            if (pass == group_pass)
            {
                workers.insert(std::make_pair(connection_id, worker{}));
                s.send(connection_id, create_cmd(LOGGED_IN, 0, timeout));
                std::cout << "connection: " << connection_id << " logged into server" << std::endl; //TODO: debug cout
            }

            else
            {
                s.send(connection_id, create_cmd(WRONG_PASS));
                s.close_connection(connection_id);
                std::cout << "connection: " << connection_id << " failed to log into server" << std::endl; //TODO: debug cout
            }

            unknown_workers.erase(it2);
        }
    }

    else
    {
        char cmd = 0;
        int task_id, param1, param2;
        int already_read = 0;

        for (int i = it->second.tmp_pos; i < std::min(12, data_len); ++i, ++already_read, ++it->second.tmp_pos)
            it->second.tmp_buffer[i] = buffer[i];

        if (it->second.tmp_pos == 12)
        {
            std::tie(cmd, task_id, param1, param2) = read_cmd(it->second.tmp_buffer);
            it->second.tmp_pos = 0;
        }

        switch (cmd)
        {
            case PLS_DONT_KILL_ME:
                break;

            case SENDING_ANSWER:
            {
                if (param1 > 0)
                {
                    auto task_it = tasks.find(task_id);
                    auto& data = task_it->second;

                    int result_file = open(data.result_path.c_str(), O_RDWR | O_CREAT | O_TRUNC, 0600);

                    if (result_file != -1)
                    {
                        auto bytes_written = write(result_file, &buffer[already_read], data_len - already_read);
                        s.send(connection_id, create_cmd(READY_FOR_ANSWER, task_id));
                        it->second.task_handled = task_id;

                        return std::make_pair(result_file, param1 - (bytes_written > 0 ? bytes_written : 0));
                    }

                    else
                    {
                        s.send(connection_id, create_cmd(ABORT_SENDING, task_id));

                        auto task_it = tasks.find(task_id);

                        if (task_it != tasks.end())
                        {
                            msg_que.push_back(task_it->second.name + ": Failed to open result file. Aborting task");
                            tasks.erase(task_it);
                        }
                    }
                }

                else if (param1 == 0)
                {
                    s.send(connection_id, create_cmd(READY_FOR_ANSWER, task_id));

                    auto task_it = tasks.find(task_id);

                    if (task_it != tasks.end())
                    {
                        msg_que.push_back("Task " + task_it->second.name + " finished!");
                        tasks.erase(task_it);
                    }
                }

                break;
            }

            case PROGRAM_FAILED:
            {
                auto task_it = tasks.find(task_id);

                if (task_it != tasks.end())
                {
                    msg_que.push_back(task_it->second.name + ": Program crashed. Aborting task");
                    tasks.erase(task_it);
                }

                break;
            }
        }

    }

    return std::make_pair(-1, -1);
}

void Scheduler::dead_connection_handler(const std::vector<int>& connections)
{
    for (auto i : connections)
    {
        auto it = workers.find(i);

        if (it != workers.end())
        {
            for (auto task_id : it->second.tasks)
                tasks_todo.push_back(task_id);

            workers.erase(it);

            std::cout << "connection " << i << " died" << std::endl;
        }

        //just in case
        auto it2 = unknown_workers.find(i);

        if (it2 != unknown_workers.end())
            unknown_workers.erase(it2);
    }
}

void Scheduler::task_done_handler(int connection_id)
{
    auto it = workers.find(connection_id);

    if (it != workers.end())
    {
        int task_id = it->second.task_handled;
        it->second.task_handled = -1;

        auto task_handle_to_delete = std::find(it->second.tasks.begin(), it->second.tasks.end(), task_id);
        it->second.tasks.erase(task_handle_to_delete);

        auto task_it = tasks.find(task_id);

        if (task_it != tasks.end())
        {
            msg_que.push_back("Task " + task_it->second.name + " completed");
            tasks.erase(task_it);
        }

        std::cout << "task " << task_id << " finished" << std::endl;
    }
}

Scheduler::Scheduler(Server &connection_handler, int timeout) : s(connection_handler), timeout(timeout)
{
    auto in_handler = [this](int connection_id, const char* buffer, int data_len){ return incoming_data_handler(connection_id, buffer, data_len); };
    auto dead_handler = [this](const std::vector<int>& connections) { return dead_connection_handler(connections); };
    auto file_handler = [this](int connection_id) { return task_done_handler(connection_id); };

    s.set_incoming_data_handler(in_handler);
    s.set_dead_connection_handler(dead_handler);
    s.set_file_end_handler(file_handler);
}

void Scheduler::add_new_job(const std::string& prog_path, const std::string& cmd_line, const std::string& task_name)
{
    task new_task;

    new_task.name = task_name;
    new_task.program_path = prog_path;
    new_task.cmd_line = cmd_line;
    new_task.result_path = prog_path + ".result";
    new_task.program = -1;
    new_task.result = -1;

    int prog = open(prog_path.c_str(), O_RDONLY);

    if (prog != -1)
    {
        new_task.program = prog;

        tasks.insert(std::make_pair(task_counter, std::move(new_task)));
        tasks_todo.push_back(task_counter++);

        msg_que.push_back("Task " + task_name + " successfully added");
    }

    else
        msg_que.push_back("Failed to add task " + task_name);
}

void Scheduler::set_password(const std::string& pass)
{
    group_pass = pass;
}

void Scheduler::update()
{
    if (workers.size() == 0)
    {
        if (tasks_todo.size() > 0)
            ++timeout_counter;

        if (timeout_counter == IDLE_TIMEOUT)
            msg_que.push_back("Error: No workers avaible for tasks!");

        return;
    }

    timeout_counter = 0;

    for (auto id : tasks_todo)
        schedule_task(id);

    tasks_todo.clear();
}

const std::vector<std::string> &Scheduler::get_messages() const
{
    return msg_que;
}

void Scheduler::clear_msg_que()
{
    msg_que.clear();
}

int Scheduler::get_workers_num() const
{
    return workers.size();
}

int Scheduler::get_tasks_num() const
{
    return tasks.size();
}
