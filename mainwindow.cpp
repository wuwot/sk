#include <QMessageBox>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    port_checker = new QIntValidator(0, 65536);
    timeout_checker = new QIntValidator(0, 7200);

    ui->port_field->setPlaceholderText("Port");
    ui->port_field->setValidator(port_checker);
    ui->port_field->setToolTip("Server port (max 65536)");

    ui->pass_field->setPlaceholderText("Password");
    ui->pass_field->setToolTip("Group password (max 32 characters)");

    ui->timeout_field->setPlaceholderText("Timeout");
    ui->timeout_field->setValidator(timeout_checker);
    ui->timeout_field->setToolTip("Timeout for inactive client in seconds (max 7200)");

    connect(ui->create_button, SIGNAL(released()), this, SLOT(create_server_click()));
    connect(ui->task_button, SIGNAL(released()), this, SLOT(add_task_click()));

    auto wrapper = [this](const std::vector<std::string> &info) { print_callback(info); };
    auto wrapper2 = [this](int num1, int num2) { set_active_workers(num1, num2); };
    worker.attach_print_callback(wrapper);
    worker.attach_workers_num_callback(wrapper2);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete port_checker;
    delete timeout_checker;
}

void MainWindow::print_callback(const std::vector<std::string> &info)
{
    for (auto& i : info)
        ui->text_output->append(QString::fromStdString(i));
}

void MainWindow::set_active_workers(int workers_num, int tasks_num)
{
    ui->workers_amount->setText(QString::number(workers_num));
    ui->task_num_label->setText(QString::number(tasks_num));
}

void MainWindow::create_server_click()
{
    if (worker.is_ready())
    {
        QMessageBox::warning(this, "Error", "Server is working already!");
        return;
    }

    auto port = ui->port_field->text();
    auto password = ui->pass_field->text();
    auto timeout = ui->timeout_field->text();

    if (port.length() == 0 || password.length() == 0 || timeout.length() == 0)
    {
        QMessageBox::warning(this, "Error", "Missing password, port or timeout!");
        return;
    }

    auto error = worker.setup(port.toInt(), timeout.toInt(), password.toStdString());

    if (error.length() == 0)
    {
        ui->port_field->setDisabled(true);
        ui->pass_field->setDisabled(true);
        ui->timeout_field->setDisabled(true);
    }

    else
        QMessageBox::warning(this, "Error", QString::fromStdString(error));
}

void MainWindow::add_task_click()
{
    if (!worker.is_ready())
    {
        QMessageBox::warning(this, "Error", "Server is not working!");
        return;
    }

    TaskSelector select;
    auto result = select.exec();

    if (result == QDialog::Accepted)
    {
        if (select.v_prog_path.length() > 0 && select.v_cmd_line.length() > 0 && select.v_task_name.length() > 0)
            worker.add_task(select.v_prog_path, select.v_cmd_line, select.v_task_name);
    }
}
