#include <QFileDialog>

#include "taskselector.h"
#include "ui_taskselector.h"

TaskSelector::TaskSelector(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TaskSelector)
{
    ui->setupUi(this);

    ui->program_path->setPlaceholderText("Program path (double click to select)");
    ui->cmd_line->setPlaceholderText("Command line");
    ui->task_name->setPlaceholderText("Task name");

    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept_click()));
    connect(ui->program_path, SIGNAL(selectionChanged()), this, SLOT(select_prog_click()));
}

TaskSelector::~TaskSelector()
{
    delete ui;
}

void TaskSelector::accept_click()
{
    v_cmd_line = ui->cmd_line->toPlainText().toStdString();
    v_task_name = ui->task_name->text().toStdString();
}

void TaskSelector::select_prog_click()
{
    QFileDialog select(this);
    select.setFileMode(QFileDialog::ExistingFile);

    if (select.exec())
    {
        auto files = select.selectedFiles();
        ui->program_path->setText(files.first());
        v_prog_path = files.first().toStdString();
    }
}
