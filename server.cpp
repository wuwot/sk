#include <iostream>
#include <sys/sendfile.h>
#include <sys/errno.h>
#include <sys/timerfd.h>
#include <cassert>
#include <string.h>

#include "server.h"

Server::~Server()
{
    close(epoll_handle);
    close(input_sock);
}

std::string Server::init(int port, int timeout)
{
    input_sock = socket(PF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);

    if (input_sock == -1)
        return std::string("socket error: ") + strerror(errno);

    sockaddr_in my_data = {};
    my_data.sin_addr.s_addr = INADDR_ANY;
    my_data.sin_port = htons(port);
    my_data.sin_family = AF_INET;

    if (bind(input_sock, (sockaddr*)&my_data, sizeof(my_data)) == -1)
        return std::string("bind error: ") + strerror(errno);

    if (listen(input_sock, LISTEN_QUEUE_SIZE) == -1)
        return std::string("listen error: ") + strerror(errno);

    epoll_handle = epoll_create1(0);

    if (epoll_handle == -1)
        return std::string("epoll_create error: ") + strerror(errno);

    epoll_event tmp_event = {};
    tmp_event.events = EPOLLIN | EPOLLET;
    tmp_event.data.fd = input_sock;

    if (epoll_ctl(epoll_handle, EPOLL_CTL_ADD, input_sock, &tmp_event) == -1)
        return std::string("epoll_ctl error: ") + strerror(errno);

    timer_handle = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);

    if (timer_handle == -1)
        return std::string("timerfd_create:") + strerror(errno);

    itimerspec timer_data = {};
    timer_data.it_value.tv_nsec = 1000;
    timer_data.it_interval.tv_sec = timeout;

    if (timerfd_settime(timer_handle, 0, &timer_data, nullptr) == -1)
        return std::string("timerfd_settime:") + strerror(errno);

    epoll_event tmp_event2 = {};
    tmp_event2.events = EPOLLIN | EPOLLET;
    tmp_event2.data.fd = timer_handle;

    if (epoll_ctl(epoll_handle, EPOLL_CTL_ADD, timer_handle, &tmp_event2) == -1)
        return std::string("epoll_ctl error: ") + strerror(errno);

    return std::string();
}

void Server::check_epoll()
{
    epoll_event events[EVENT_BUFFER_SIZE] = {};

    auto ret = epoll_wait(epoll_handle, events, EVENT_BUFFER_SIZE, EPOLL_WAIT_TIME);

    if (ret > 0)
    {
        for (int i = 0; i < ret; ++i)
        {
            auto sock = events[i].data.fd;

            if (events[i].events & (EPOLLIN | EPOLLRDHUP)) //some data is send/new connection is pending/socket got closed
            {
                if (sock != input_sock)
                    rec_que.push_back(sock);
            }

            if (events[i].events & EPOLLOUT) //socket is ready for sending new data
            {
                auto it = connections.find(sock);

                if (it != connections.end() && !it->second.out_idle)
                    send_que.push_back(sock);
            }

            if (events[i].events & (EPOLLERR | EPOLLHUP)) //some error occured
                delete_que.push_back(sock);
        }
    }

    else if (ret == -1 && errno != EINTR)
    {
        perror("Epoll error: ");
        exit(-1);
    }
}

void Server::check_for_new_connections()
{
    int new_worker = 0;

    do
    {
        sockaddr_in worker_data = {};
        unsigned sock_len = sizeof(worker_data);
        new_worker = accept(input_sock, (sockaddr*)&worker_data, (socklen_t*)&sock_len);

        if (new_worker > 0)
        {
            if (fcntl(new_worker, F_SETFL, fcntl(new_worker, F_GETFL, 0) | O_NONBLOCK) == -1)
            {
                perror("failed to set new connection as non blocking: ");
                continue;
            }

            epoll_event tmp_event = {};
            tmp_event.events = EPOLLIN | EPOLLOUT | EPOLLET | EPOLLRDHUP;
            tmp_event.data.fd = new_worker;

            if (epoll_ctl(epoll_handle, EPOLL_CTL_ADD, new_worker, &tmp_event) == -1)
            {
                perror("epoll_ctl error: ");
                continue;
            }

            connections.insert(std::make_pair(new_worker, connection(new_worker)));

            std::cout << "new connection: " << new_worker << " ip: " << inet_ntoa(worker_data.sin_addr) << std::endl; //TODO: debug cout
        }

    } while (new_worker != -1);

    if (errno != EAGAIN && errno != EWOULDBLOCK)
        perror("Accept error: ");
}

void Server::check_timer()
{
    char tmp_buf[8];
    int dummy = read(timer_handle, tmp_buf, 8);

    if (dummy > 0)
        timeout_routine();

    else if (dummy == -1 && (errno != EWOULDBLOCK || errno != EAGAIN))
        perror("read from timer: ");
}

void Server::timeout_routine()
{
    for (auto& i : connections)
    {
        if (i.second.active)
            i.second.active = false;

        else
        {
            delete_que.push_back(i.first);
            std::cout << "Connection " << i.first << " timeout!" << std::endl;
        }
    }
}

void Server::handle_incoming_data(connection& con)
{
    int bytes_saved = 0;
    int bytes_written = 0;
    int bytes_to_process = 0;
    int bytes_processed = 0;
    int file_desc = 0;
    int file_size = 0;
    char buffer[REC_BUFFER_SIZE];

    CORO_BEGIN(con.in_ctx);

    while (true)
    {
        if (!con.rec_data.using_handle)
        {
            bytes_to_process = CMD_BUFFER_SIZE;
            bytes_processed = read(con.sock, con.rec_data.cmd_buffer, bytes_to_process);
        }

        else
        {
            bytes_to_process = std::min(con.rec_data.total_size - con.rec_data.processed_size, con.rec_data.chunk_size);
            bytes_processed = read(con.sock, buffer, REC_BUFFER_SIZE);
        }

        if (bytes_processed == 0) //connection closed
        {
            delete_que.push_back(con.sock);
            CORO_ABORT();
        }

        else if (bytes_processed == -1)
        {
            if (errno != EWOULDBLOCK && errno != EAGAIN) //error occured
            {
                delete_que.push_back(con.sock);
                CORO_ABORT();
            }

            else
            {
                con.in_idle = false;
                CORO_YIELD();
            }
        }

        else
        {
            con.active = true;

            if (con.rec_data.using_handle)
            {               
                con.rec_data.processed_size += bytes_processed;

                while (bytes_saved < bytes_processed)
                {
                    bytes_written = write(con.rec_data.handle, buffer, bytes_processed - bytes_saved);

                    if (bytes_written > 0)
                        bytes_saved += bytes_written;

                    else if (bytes_written == -1 && errno != EINTR) //some critical fail
                    {
                        perror("writing to recive file");
                        exit(-1);
                        break;
                    }
                }

                if (con.rec_data.total_size == con.rec_data.processed_size)
                {
                    close(con.rec_data.handle);
                    con.rec_data.handle = -1;
                    con.rec_data.total_size = 0;
                    con.rec_data.processed_size = 0;
                    con.rec_data.chunk_size = 0;

                    con.in_idle = true;
                    con.rec_data.using_handle = false;

                    file_rec_end_callback(con.sock);

                    break;
                }
            }

            else
            {
                std::tie(file_desc, file_size) = cmd_buff_callback(con.sock, con.rec_data.cmd_buffer, bytes_processed);

                if (file_desc != -1)
                {
                    con.rec_data.handle = file_desc;
                    con.rec_data.chunk_size = FILE_CHUNK_SIZE;
                    con.rec_data.processed_size = 0;
                    con.rec_data.total_size = file_size;

                    con.rec_data.using_handle = true;
                }
            }
        }
    }

    CORO_END();
}

void Server::handle_outgoing_data(connection& con)
{
    int bytes_to_process = 0;
    int bytes_processed = 0;

    CORO_BEGIN(con.out_ctx);

    while (true)
    {
        bytes_to_process = std::min(con.send_que.front().total_size - con.send_que.front().processed_size, con.send_que.front().chunk_size);

        switch(con.send_que.front().send_type)
        {
            case OUT_CMD:
                bytes_processed = write(con.sock, &con.send_que.front().cmd_buffer[con.send_que.front().processed_size], bytes_to_process);
                break;

            case OUT_PTR:
                bytes_processed = write(con.sock, con.send_que.front().ptr + con.send_que.front().processed_size, bytes_to_process);
                break;

            case OUT_HANDLE:
                bytes_processed = sendfile(con.sock, con.send_que.front().handle, nullptr, bytes_to_process);
                break;
        }

        if (bytes_processed == 0) //connection closed
        {
            delete_que.push_back(con.sock);
            CORO_ABORT();
        }

        else if (bytes_processed == -1)
        {
            if (errno != EWOULDBLOCK && errno != EAGAIN) //error occured
            {
                delete_que.push_back(con.sock);
                CORO_ABORT();
            }

            else
            {
                con.out_idle = false;
                CORO_YIELD();
            }
        }

        else
        {
            con.send_que.front().processed_size += bytes_processed;

            if (con.send_que.front().processed_size == con.send_que.front().total_size)
            {
                con.send_que.pop();

                if (con.send_que.size() == 0)
                {
                    con.out_idle = true;
                    break;
                }
            }
        }
    }

    CORO_END();
}

void Server::main_loop()
{
    check_epoll();
    check_for_new_connections();

    for (auto i : rec_que)
    {
        auto it = connections.find(i);

        if (it != connections.end())
           handle_incoming_data(it->second);
    }

    rec_que.clear();

    for (auto i : send_que)
    {
        auto it = connections.find(i);

        if (it != connections.end())
           handle_outgoing_data(it->second);
    }

    send_que.clear();

    check_timer(); //give connection last chance

    for (auto i : delete_que)
    {
        auto it = connections.find(i);

        if (it != connections.end())
            connections.erase(it);
    }

    if (delete_que.size() > 0)
        dead_con_callback(delete_que);

    delete_que.clear();
}

void Server::send(int connection_id, const std::array<char, 12>& cmd)
{
    auto it = connections.find(connection_id);
    assert(it != connections.end());

    out_data tmp;
    std::copy(cmd.begin(), cmd.end(), std::begin(tmp.cmd_buffer));
    tmp.send_type = OUT_CMD;
    tmp.chunk_size = cmd.size();
    tmp.processed_size = 0;
    tmp.total_size = cmd.size();

    it->second.send_que.push(tmp);

    if (it->second.send_que.size() == 1)
        send_que.push_back(connection_id);
}

void Server::send(int connection_id, int handle, int size)
{
    auto it = connections.find(connection_id);
    assert(it != connections.end());

    out_data tmp;
    tmp.handle = handle;
    tmp.send_type = OUT_HANDLE;
    tmp.chunk_size = FILE_CHUNK_SIZE;
    tmp.processed_size = 0;
    tmp.total_size = size;

    it->second.send_que.push(tmp);

    if (it->second.send_que.size() == 1)
        send_que.push_back(connection_id);
}

void Server::send(int connection_id, const char *ptr, int size)
{
    auto it = connections.find(connection_id);
    assert(it != connections.end());

    out_data tmp;
    tmp.ptr = ptr;
    tmp.send_type = OUT_PTR;
    tmp.chunk_size = size;
    tmp.processed_size = 0;
    tmp.total_size = size;

    it->second.send_que.push(tmp);

    if (it->second.send_que.size() == 1)
        send_que.push_back(connection_id);
}

void Server::set_incoming_data_handler(std::function<std::pair<int, int>(int, const char*, int)> handler)
{
    cmd_buff_callback = handler;
}

void Server::set_dead_connection_handler(std::function<void (const std::vector<int> &)> handler)
{
    dead_con_callback = handler;
}

void Server::set_file_end_handler(std::function<void (int)> handler)
{
    file_rec_end_callback = handler;
}

void Server::close_connection(int connection_id)
{
    auto it = connections.find(connection_id);

    if (it != connections.end())
        connections.erase(it);
}
