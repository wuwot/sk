#include "worker.h"

void Worker::worker_loop()
{
    while (running)
    {
        sched->update();
        serv->main_loop();

        std::unique_lock<std::mutex> lock(worker_lock);

        for (auto& i : task_que)
            sched->add_new_job(std::get<0>(i), std::get<1>(i), std::get<2>(i));

        task_que.clear();

        num_callback(sched->get_workers_num(), sched->get_tasks_num());
        gui_callback(sched->get_messages());
        sched->clear_msg_que();
    }
}

void Worker::attach_print_callback(std::function<void (const std::vector<std::string> &)> print_handler)
{
    gui_callback = print_handler;
}

void Worker::attach_workers_num_callback(std::function<void (int, int)> handler)
{
    num_callback = handler;
}

Worker::~Worker()
{
    running.store(false);

    if (worker_thread.joinable())
        worker_thread.join();
}

bool Worker::is_ready()
{
    return serv && sched;
}

std::string Worker::setup(int port, int timeout, const std::string &password)
{
    serv = std::make_unique<Server>();
    std::string error_str = serv->init(port, timeout);

    if (error_str.length() > 0)
        serv.reset();

    else
    {
        sched = std::make_unique<Scheduler>(*serv, timeout);
        sched->set_password(password);

        running.store(true);
        worker_thread = std::thread(&Worker::worker_loop, this);
    }

    return error_str;
}

void Worker::add_task(const std::string &prog_name, const std::string &cmd_line, const std::string& name)
{
    std::unique_lock<std::mutex> lock(worker_lock);
    task_que.push_back(std::make_tuple(prog_name, cmd_line, name));
}
