#ifndef WORKER_H
#define WORKER_H

#include <string>
#include <memory>
#include <thread>
#include <mutex>
#include <vector>
#include <tuple>
#include <atomic>
#include <functional>

#include "server.h"
#include "scheduler.h"

class Worker
{
    private:
        std::thread worker_thread;
        std::mutex worker_lock;

        std::unique_ptr<Server> serv;
        std::unique_ptr<Scheduler> sched;

        std::vector<std::tuple<std::string, std::string, std::string>> task_que;

        std::function<void(const std::vector<std::string>&)> gui_callback;
        std::function<void(int, int)> num_callback;

        std::atomic<bool> running;

        void worker_loop();

    public:
        ~Worker();

        bool is_ready();
        std::string setup(int port, int timeout, const std::string &password);
        void add_task(const std::string& prog_name, const std::string& cmd_line, const std::string &name);
        void attach_print_callback(std::function<void(const std::vector<std::string>&)> print_handler);
        void attach_workers_num_callback(std::function<void(int, int)> handler);
};

#endif // WORKER_H
