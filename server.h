#ifndef SERVER_H
#define SERVER_H

#include <functional>
#include <unordered_map>
#include <vector>
#include <queue>

#include <sys/epoll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>

#include "coro.h"

constexpr int EPOLL_WAIT_TIME = 500;
constexpr int EVENT_BUFFER_SIZE = 10;
constexpr int LISTEN_QUEUE_SIZE = 25;
constexpr int CMD_BUFFER_SIZE = 12;
constexpr int FILE_CHUNK_SIZE = 8192;
constexpr int REC_BUFFER_SIZE = 8192;

enum OUT_TYPE { OUT_CMD, OUT_PTR, OUT_HANDLE };

struct out_data
{
    union { char cmd_buffer[12]; const char* ptr; int handle; };
    OUT_TYPE send_type = OUT_CMD;

    int total_size = 0;
    int chunk_size = 0;
    int processed_size = 0;
};

struct in_data
{
    char cmd_buffer[CMD_BUFFER_SIZE];

    int handle = -1;
    int total_size = 0;
    int chunk_size = 0;
    int processed_size = 0;

    bool using_handle = false;
};

struct connection
{
    std::queue<out_data> send_que;
    in_data rec_data;

    coro_ctx in_ctx = coro_init;
    coro_ctx out_ctx = coro_init;

    int sock = -1;

    bool in_idle = true;
    bool out_idle = true;
    bool active = true;

    connection(int s = -1) : sock(s) {}
    ~connection() { close(sock); }

    connection(const connection&) = delete;
    connection& operator=(connection&) = delete;

    connection(connection&& other) : send_que(std::move(other.send_que)), in_ctx(other.in_ctx),
        out_ctx(other.out_ctx), sock(other.sock), in_idle(other.in_idle), out_idle(other.out_idle)
    {
        other.sock = -1;
        other.in_idle = true;
        other.out_idle = true;
    }

    connection& operator=(connection&& other)
    {
        if(this != &other)
        {
            std::swap(*this, other);

            other.sock = -1;
            other.in_idle = true;
            other.out_idle = true;
        }

        return *this;
    }
};

class Server
{
    private:
        std::unordered_map<int, connection> connections;

        std::vector<int> send_que;
        std::vector<int> rec_que;
        std::vector<int> delete_que;

        std::function<std::pair<int, int>(int, const char*, int)> cmd_buff_callback;
        std::function<void(const std::vector<int>&)> dead_con_callback;
        std::function<void(int)> file_rec_end_callback;

        int input_sock = -1;
        int epoll_handle = -1;
        int timer_handle = -1;

        void check_epoll();
        void check_for_new_connections();
        void check_timer();
        void timeout_routine();
        void handle_incoming_data(connection& con);
        void handle_outgoing_data(connection& con);
        void send_data(out_data& data, int sock);

    public:
        Server() {}
        ~Server();

        std::string init(int port, int timeout);
        void main_loop();
        void send(int connection_id, const std::array<char,12>& cmd);
        void send(int connection_id, int handle, int size);
        void send(int connection_id, const char* ptr, int size);
        void set_incoming_data_handler(std::function<std::pair<int, int>(int, const char*, int)> handler);
        void set_dead_connection_handler(std::function<void(const std::vector<int>&)> handler);
        void set_file_end_handler(std::function<void(int)> handler);
        void close_connection(int connection_id);
};

#endif // SERVER_H
