#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <array>
#include <vector>
#include <string>
#include <unordered_map>
#include <functional>
#include "server.h"

constexpr char LOGGED_IN = 'i';
constexpr char WRONG_PASS = 'w';
constexpr char SENDING_TASK = 't';
constexpr char ANSWER_ACK = 'x';
constexpr char READY_FOR_ANSWER = 'r';
constexpr char ABORT_SENDING = 'a';

constexpr char TRY_LOGIN = 'o';
constexpr char SENDING_ANSWER = 's';
constexpr char PROGRAM_FAILED = 'f';
constexpr char PLS_DONT_KILL_ME = 'p';

constexpr int IDLE_TIMEOUT = 480;

struct new_worker
{
    std::string pass;

    std::array<char, 12> tmp_buffer;
    int tmp_pos = 0;
    unsigned pass_len = 0;

    bool rec_pass;
};

struct worker
{
    std::vector<int> tasks;
    std::array<char, 12> tmp_buffer;
    int tmp_pos = 0;
    int task_handled = -1;
};

struct task
{
    std::string name;
    std::string program_path;
    std::string cmd_line;
    std::string result_path;

    std::array<char, 12> cmd_buffer;

    int program = -1;
    int result = -1;

    task(const task&) = delete;
    task& operator=(const task&) = delete;

    task() {}

    task(task&& other) : name(std::move(other.name)), program_path(std::move(other.program_path)), cmd_line(std::move(other.cmd_line)),
        result_path(std::move(other.result_path)), cmd_buffer(std::move(other.cmd_buffer)), program(other.program), result(other.result)
    {
        other.program = -1;
        other.result = -1;
    }

    task& operator= (task&& other)
    {
        if (this != &other)
        {
            close(program);
            close(result);

            program_path = std::move(other.program_path);
            cmd_line = std::move(other.cmd_line);
            result_path = std::move(other.result_path);
            cmd_buffer = std::move(other.cmd_buffer);
            program = other.program;
            result = other.result;
            name = std::move(other.name);

            other.program = -1;
            other.result = -1;
        }

        return *this;
    }

    ~task()
    {
        close(program);
        close(result);
    }
};

class Scheduler
{    
    Server& s;

    std::unordered_map<int, new_worker> unknown_workers;
    std::unordered_map<int, worker> workers;

    std::unordered_map<int, task> tasks;
    std::vector<int> tasks_todo;
    std::vector<std::string> msg_que;

    std::string group_pass;

    int task_counter = 0;
    int timeout = 0;
    int timeout_counter = 0;

    private:
        void schedule_task(int task_id);
        std::pair<int, int> incoming_data_handler(int connection_id, const char* buffer, int data_len);
        void dead_connection_handler(const std::vector<int>& connections);
        void task_done_handler(int connection_id);

    public:
        Scheduler(Server& connection_handler, int timeout);
        void add_new_job(const std::string& prog_path, const std::string& cmd_line, const std::string& task_name);
        void set_password(const std::string& pass);
        void update();

        const std::vector<std::string>& get_messages() const;
        void clear_msg_que();
        int get_workers_num() const;
        int get_tasks_num() const;
};

#endif // SCHEDULER_H
